uNote
=====

uNote is an android application, it's a lightweight and minimalist notepad. uNote is for "micro Note"
It focus on simplicity and give only basic features:

* add/delete note,
* secure a note with password (since version 1.1.0)
* sort notes by create date, modification date or title
* Search notes (only title or also in content except the ones password protected)
* Export/Import database to sdcard

Attention: the database may be cleaned if you update from 1.0.X to 1.1.X

Thanks for Chris Orj for the german translation and Andrés Hernández for the spanish one.


The icon of the application has been taken for openclipart.org, [here the link] (https://openclipart.org/detail/190827/feuille-de-carnet-sheet-book)


Here some screenshots:

![Alt text](img/main.png?raw=true "Main activity empty")
The main screen 

![Alt text](img/preferences.png?raw=true "Preferences")
the preferences screen

![Alt text](img/noteEdition.png?raw=true "Create note")
Creating a note

![Alt text](img/listNote.png?raw=true "Main activity with one note")
The main note with the note created
